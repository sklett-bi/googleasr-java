package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import asr.GoogleASR;

public class ASRServer implements Runnable {

	private GoogleASR asr;
	private ServerSocket server;

	public ASRServer(GoogleASR asr) {
		this.asr = asr;
	}

	public void startServer() throws IOException {

		this.server = new ServerSocket(8080);
		Thread thread = new Thread(this);
		thread.setDaemon(false);
		thread.start();

	}

	@Override
	public void run() {
		while (true) {
			Socket s;
			try {
				s = this.server.accept();

				BufferedReader bufferedReader;

				bufferedReader = new BufferedReader(new InputStreamReader(s.getInputStream(),
						"UTF8"));

				char[] buffer = new char[4 * 1024];
				bufferedReader.read(buffer, 0, 4 * 1024);

				String request = new String(buffer);
				request = request.trim();

				asr.parseAjaxRequest(request);

				String html = "";
				File f = new File("index.html");
				html = new String(Files.readAllBytes(f.toPath()), StandardCharsets.UTF_8);
				OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
				out.write(html);
				out.flush();
				out.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

}
