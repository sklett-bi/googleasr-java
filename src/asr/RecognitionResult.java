package asr;

public class RecognitionResult {

	String[] hypotheses;
	Double[] confidences;
	Boolean state;

	public String[] getHypotheses() {
		return hypotheses;
	}

	public void setHypotheses(String[] hypotheses) {
		this.hypotheses = hypotheses;
	}

	public Double[] getConfidences() {
		return confidences;
	}

	public void setConfidences(Double[] confidences) {
		this.confidences = confidences;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

}
