package asr;

import java.awt.Desktop;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.Arrays;

import server.ASRServer;

public class GoogleASR {

	public GoogleASR() {
		ASRServer server = new ASRServer(this);
		try {
			server.startServer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param recognitionResult
	 *            object holding the ASR-Result information after being<br>
	 *            parsed
	 */
	public void processResult(RecognitionResult recognitionResult) {

		printResults(recognitionResult);

	}

	private void printResults(RecognitionResult recognitionResult) {

		System.out.println("--- printing result ---");
		System.out.println(Arrays.toString(recognitionResult.hypotheses));
		System.out.println(Arrays.toString(recognitionResult.confidences));
		System.out.println("final: " + recognitionResult.getState());

	}

	public void openBrowser() {

		new Thread() {
			public void run() {
				if (Desktop.isDesktopSupported()) {
					try {
						Desktop.getDesktop().browse(new URI("http://localhost:8080"));
					} catch (IOException e) {
						e.printStackTrace();
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();

	}

	public void parseAjaxRequest(String request) throws UnsupportedEncodingException {

		for (String line : request.split(System.lineSeparator())) {
			if (line.contains("recog")) {
				line = URLDecoder.decode(line, "UTF-8");
				line = line.trim();

				String[] recogResults = new String[line.split("recognitionResults\\[\\]=").length - 1];
				int i = 0;
				for (String recogResult : line.split("recognitionResults\\[\\]=")) {
					if (recogResult.length() == 0)
						continue;

					String result = recogResult.substring(0, recogResult.indexOf("&")).trim();

					if (result.length() > 0) {
						recogResults[i] = result;
						i++;
					}
				}
				Double[] confidences = new Double[line.split("confidences\\[\\]=").length - 1];
				int j = 0;
				for (String confidence : line.split("confidences\\[\\]=")) {
					if (confidence.length() == 0 || confidence.contains("recog"))
						continue;

					String result = confidence.substring(0, confidence.indexOf("&")).trim();

					if (result.length() > 0) {
						confidences[j] = Double.parseDouble(result);
						j++;
					}
				}

				boolean finalHypo = false;

				if (line.split("finalHypo=").length > 1)
					finalHypo = line.split("finalHypo=")[1].equals("true");

				RecognitionResult rr = new RecognitionResult();
				rr.setHypotheses(recogResults);
				rr.setConfidences(confidences);
				rr.setState(finalHypo);
				processResult(rr);
			}
		}
	}
}
