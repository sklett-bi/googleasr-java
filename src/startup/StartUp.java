package startup;

import asr.GoogleASR;

public class StartUp {

	public static void main(String[] args) {
		
		System.out.println("starting googleASR...");
		GoogleASR asr = new GoogleASR();
		System.out.println("open chrome/chromium, go to localhost:8080 and click start");
		while(true) {
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
